package com.example.sibintekbackend.core.controllers;

import com.example.sibintekbackend.core.domain.factories.ServiceEnum;
import com.example.sibintekbackend.core.domain.factories.ServiceFactory;
import com.example.sibintekbackend.core.domain.services.AuthorService;
import com.example.sibintekbackend.data.entities.AuthorBooks;
import com.example.sibintekbackend.data.entities.BookAuthors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthorController {
    AuthorService aService;

    @RequestMapping(path = "/author",
            method = RequestMethod.GET)
    public List<AuthorBooks> getAuthorFromFirstName(@RequestParam(value = "firstName",
            required = false, defaultValue = "") String firstName) {
        aService = ServiceFactory.createService(ServiceEnum.AUTHOR);
        List<AuthorBooks> authorBooks = aService.findByFirstName(firstName);
        for(int i = 0; i < authorBooks.size(); i++) {
            for(int j = 0; j < authorBooks.get(i).getBooks().size(); j++) {
                authorBooks.get(i).getBooks().get(j).setAuthors(null);
            }
        }
        return authorBooks;
    }

    @RequestMapping(path = "/authors",
            method = RequestMethod.GET)
    public List<AuthorBooks> getAllAuthors() {
        aService = AuthorService.getAuthorService();
        List<AuthorBooks> authorBooks = aService.findAllUnique();
        for(int i = 0; i < authorBooks.size(); i++) {
            authorBooks.get(i).setBooks(null);
        }
        return authorBooks;
    }

    @RequestMapping(path = "/authorsId",
            method = RequestMethod.GET)
    public List<AuthorBooks> getAuthorsById(@RequestParam(value = "first",
            required = false, defaultValue = "") int firstId, @RequestParam(value = "second",
            required = false, defaultValue = "") int secondId) {
        aService = AuthorService.getAuthorService();
        List<AuthorBooks> authorBooks = aService.findAuthorsById(firstId, secondId);
        for(int i = 0; i < authorBooks.size(); i++) {
            authorBooks.get(i).setBooks(null);
        }
        return authorBooks;
    }

    @RequestMapping(path = "/authorBooks",
            method = RequestMethod.GET)
    public List<BookAuthors> getAuthorBooks(@RequestParam(value = "firstName",
            required = false, defaultValue = "") String firstName, @RequestParam(value = "secondName",
            required = false, defaultValue = "") String secondName) {
        aService = AuthorService.getAuthorService();
        List<BookAuthors> bookAuthors = aService.findByFirstNameAndSecondName(firstName, secondName);
        for(int i = 0; i < bookAuthors.size(); i++) {
            bookAuthors.get(i).setAuthors(null);
        }
        return bookAuthors;
    }
}
