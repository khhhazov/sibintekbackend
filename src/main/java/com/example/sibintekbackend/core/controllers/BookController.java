package com.example.sibintekbackend.core.controllers;

import com.example.sibintekbackend.core.domain.factories.ServiceEnum;
import com.example.sibintekbackend.core.domain.factories.ServiceFactory;
import com.example.sibintekbackend.core.domain.services.BookService;
import com.example.sibintekbackend.data.entities.BookAuthors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookController {
    BookService bService;

    @RequestMapping(path = "/book",
            method = RequestMethod.GET)
    public List<BookAuthors> getBookFromTitle(@RequestParam(value = "title",
            required = false, defaultValue = "") String title) {
        bService = ServiceFactory.createService(ServiceEnum.BOOK);
        List<BookAuthors> bookAuthors = bService.findByTitle(title);
        for(int i = 0; i < bookAuthors.size(); i++) {
            for(int j = 0; j < bookAuthors.get(i).getAuthors().size(); j++) {
                bookAuthors.get(i).getAuthors().get(j).setBooks(null);
            }
        }
        return bookAuthors;
    }
}
