package com.example.sibintekbackend.core.domain.services;

import com.example.sibintekbackend.core.usecases.author.BookRepository;
import com.example.sibintekbackend.data.entities.BookAuthors;
import com.example.sibintekbackend.data.repository.BookRepositoryImpl;

import java.util.List;

public class BookService {
    BookRepository repository = new BookRepositoryImpl();

    private static BookService bookService;
    private BookService(){}

    public static BookService getBookService() {
        if (bookService == null) {
            bookService = new BookService();
        }
        return bookService;
    }

    public List<BookAuthors> findByTitle(String title) {
        return repository.findByTitle(title);
    }

    public List<BookAuthors> findAll() {
        return null;
    }

    public BookAuthors findById(Long id) {
        return null;
    }

    public void createBook(BookAuthors entity) {
        repository.create(entity);
    }

    public void updateBook(BookAuthors entity) {

    }

    public void deleteBook(BookAuthors entity) {

    }
}
