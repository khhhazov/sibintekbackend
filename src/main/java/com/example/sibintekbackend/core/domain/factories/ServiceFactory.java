package com.example.sibintekbackend.core.domain.factories;

import com.example.sibintekbackend.core.domain.services.AuthorService;
import com.example.sibintekbackend.core.domain.services.BookService;

public class ServiceFactory {
    static public <T> T createService(ServiceEnum type) {
        return switch (type) {
            case AUTHOR -> (T) AuthorService.getAuthorService();
            case BOOK -> (T) BookService.getBookService();
        };
    }
}
