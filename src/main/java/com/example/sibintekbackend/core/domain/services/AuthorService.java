package com.example.sibintekbackend.core.domain.services;

import com.example.sibintekbackend.core.usecases.author.AuthorRepository;
import com.example.sibintekbackend.data.entities.AuthorBooks;
import com.example.sibintekbackend.data.entities.BookAuthors;
import com.example.sibintekbackend.data.repository.AuthorRepositoryImpl;

import java.util.List;

public class AuthorService {
    private final AuthorRepository repository = new AuthorRepositoryImpl();

    private static AuthorService authorService;

    private AuthorService(){}

    public static AuthorService getAuthorService() {
        if (authorService == null) {
            authorService = new AuthorService();
        }
        return authorService;
    }

    public List<AuthorBooks> findAuthorsById(int first, int second) {
        return repository.findAuthorsById(first, second);
    }

    public List<AuthorBooks> findByFirstName(String firstName) {
        return repository.findByFirstName(firstName);
    }

    public List<BookAuthors> findByFirstNameAndSecondName(String firstName, String secondName) {
        return repository.findByFirstNameAndSecondName(firstName, secondName);
    }

    public List<AuthorBooks> findAll() {
        return repository.findAll();
    }

    public List<AuthorBooks> findAllUnique() {
        return repository.findAllUnique();
    }

    public AuthorBooks findById(Long id) {
        return null;
    }

    public void createAuthor(AuthorBooks authorBooks) {
        repository.create(authorBooks);
    }

    public void deleteAuthor(AuthorBooks authorBooks) {

    }
}
