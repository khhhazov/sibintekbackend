package com.example.sibintekbackend.core.domain.utils;

import com.example.sibintekbackend.data.entities.AuthorBooks;
import com.example.sibintekbackend.data.entities.BookAuthors;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

    private HibernateUtil() {}

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(AuthorBooks.class);
                configuration.addAnnotatedClass(BookAuthors.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());

            } catch (Exception e) {
                System.out.println("exception:" + e);
            }
        }
        return sessionFactory;
    }
}
