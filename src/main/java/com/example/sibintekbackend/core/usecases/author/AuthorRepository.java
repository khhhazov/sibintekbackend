package com.example.sibintekbackend.core.usecases.author;

import com.example.sibintekbackend.data.entities.AuthorBooks;
import com.example.sibintekbackend.data.entities.BookAuthors;
import com.example.sibintekbackend.data.repository.Repository;

import java.util.List;

public interface AuthorRepository extends Repository<AuthorBooks, Long> {

    List<AuthorBooks> findAuthorsById(int first, int second);

    List<AuthorBooks> findByFirstName(String firstName);

    List<AuthorBooks> findAllUnique();

    List<BookAuthors> findByFirstNameAndSecondName(String firstName, String secondName);

}
