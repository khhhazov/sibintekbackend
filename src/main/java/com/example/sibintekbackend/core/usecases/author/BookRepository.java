package com.example.sibintekbackend.core.usecases.author;

import com.example.sibintekbackend.data.entities.BookAuthors;
import com.example.sibintekbackend.data.repository.Repository;

import java.util.List;

public interface BookRepository extends Repository<BookAuthors, Long> {
    List<BookAuthors> findByTitle(String title);
}
