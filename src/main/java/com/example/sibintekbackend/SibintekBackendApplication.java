package com.example.sibintekbackend;

import com.example.sibintekbackend.core.domain.services.AuthorService;
import com.example.sibintekbackend.data.entities.AuthorBooks;
import com.example.sibintekbackend.data.entities.BookAuthors;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SibintekBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SibintekBackendApplication.class, args);
    }
}
