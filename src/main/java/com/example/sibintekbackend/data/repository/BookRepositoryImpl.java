package com.example.sibintekbackend.data.repository;

import com.example.sibintekbackend.core.domain.utils.HibernateUtil;
import com.example.sibintekbackend.core.usecases.author.BookRepository;
import com.example.sibintekbackend.data.entities.BookAuthors;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class BookRepositoryImpl implements BookRepository {
    @Override
    public List<BookAuthors> findAll() {
        return null;
    }

    @Override
    public BookAuthors findById(Long id) {
        return null;
    }

    @Override
    public void create(BookAuthors entity) {
        Session session = HibernateUtil
                .getSessionFactory()
                .openSession();
        Transaction transaction = session
                .beginTransaction();
        session.saveOrUpdate(entity);
        transaction.commit();
        session.close();
    }

    @Override
    public void update(BookAuthors entity) {

    }

    @Override
    public void delete(BookAuthors entity) {

    }

    @Override
    public List<BookAuthors> findByTitle(String title) {
        return HibernateUtil
                .getSessionFactory()
                .openSession()
                .createQuery("SELECT b FROM BookAuthors b WHERE title LIKE " + "'" + title + "'")
                .list();
    }
}
