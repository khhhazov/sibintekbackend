package com.example.sibintekbackend.data.repository;

import java.util.List;

public interface Repository<T, I> {

    List<T> findAll();

    T findById(I id);

    void create(T entity);

    void update(T entity);

    void delete(T entity);

}
