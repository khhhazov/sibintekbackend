package com.example.sibintekbackend.data.repository;

import com.example.sibintekbackend.core.domain.utils.HibernateUtil;
import com.example.sibintekbackend.core.usecases.author.AuthorRepository;
import com.example.sibintekbackend.data.entities.AuthorBooks;
import com.example.sibintekbackend.data.entities.BookAuthors;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class AuthorRepositoryImpl implements AuthorRepository {

    @Override
    public List<AuthorBooks> findAuthorsById(int first, int second) {
        return HibernateUtil
                .getSessionFactory()
                .openSession()
                .createQuery("SELECT a FROM AuthorBooks a WHERE id BETWEEN " + first + " AND " + second)
                .list();
    }

    @Override
    public List<AuthorBooks> findByFirstName(String firstName) {
        return HibernateUtil
                .getSessionFactory()
                .openSession()
                .createQuery("SELECT a FROM AuthorBooks a WHERE firstName LIKE " + "'" + firstName + "'")
                .list();
    }

    @Override
    public List<AuthorBooks> findAllUnique() {
        List<Object[]> authors = HibernateUtil
                .getSessionFactory()
                .openSession()
                .createNativeQuery("SELECT DISTINCT ON (first_name, second_name) * FROM authors")
                .getResultList();
        List<AuthorBooks> authorBooksList = new ArrayList<>();
        for (Object[] author: authors) {
            AuthorBooks authorBooks = new AuthorBooks();

            authorBooks.setId(Integer.parseInt(author[0].toString()));
            authorBooks.setFirstName(author[1].toString());
            authorBooks.setSecondName(author[2].toString());
            authorBooks.setBooks(null);
            authorBooksList.add(authorBooks);
        }
        return authorBooksList;
    }

    @Override
    public List<BookAuthors> findByFirstNameAndSecondName(String firstName, String secondName) {
        return HibernateUtil
                .getSessionFactory()
                .openSession()
                .createQuery("SELECT bookAuthors FROM AuthorBooks WHERE firstName LIKE " +
                        "'" + firstName + "' " + "AND secondName LIKE " + "'" + secondName + "'")
                .list();
    }

    @Override
    public List<AuthorBooks> findAll() {
        return HibernateUtil
                .getSessionFactory()
                .openSession()
                .createQuery("SELECT a FROM AuthorBooks a")
                .list();
    }

    @Override
    public AuthorBooks findById(Long id) {
        return null;
    }

    @Override
    public void create(AuthorBooks entity) {
        Session session = HibernateUtil
                .getSessionFactory()
                .openSession();
        Transaction transaction = session
                .beginTransaction();
        session.saveOrUpdate(entity);
        transaction.commit();
        session.close();
    }

    @Override
    public void update(AuthorBooks entity) {

    }

    @Override
    public void delete(AuthorBooks entity) {

    }
}
