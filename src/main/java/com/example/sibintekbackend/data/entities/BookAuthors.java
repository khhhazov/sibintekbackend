package com.example.sibintekbackend.data.entities;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "books")
public class BookAuthors {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private int id;

    @Column(name = "title", unique = true)
    private String title;

    @ManyToMany(mappedBy = "bookAuthors")
    private List<AuthorBooks> authorBooks;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<AuthorBooks> getAuthors() {
        return authorBooks;
    }

    public void setAuthors(List<AuthorBooks> authorBooks) {
        this.authorBooks = authorBooks;
    }
}
